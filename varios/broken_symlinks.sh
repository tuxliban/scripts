#!/bin/sh
# Script para buscar enlaces simbólicos rotos en el directorio actual
# Si se desea buscar en todo el sistema cambiar ./ por /
#
# Dependencias: find
#
# Autor: O. Sánchez <o-sanchez@linuxmail.org>

set -e  # Si hay un error, salir inmediatamente

find ./ -type l > tmp
while IFS= read -r file
do
	cd "$(dirname "$file")" || return
	if [ ! -e "$(readlink "$(basename "$file")")" ]; then
		ls -g "$file" | awk '{print $8" "$9" "$10}'
		echo "Realpath: $(realpath "$file")"
	fi
	cd - > /dev/null || exit
done < tmp
rm tmp
