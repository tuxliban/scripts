#!/bin/sh
## Script que elimina los archivos residuales de LaTex cuando
## se compilan archivos

for _file in ./*.aux ./*.log ./*.out; do
	[ -e "$_file" ] || continue
	rm -rf "$_file"
done
