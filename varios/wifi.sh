#!/bin/sh
# Alternar estado de conexión wifi
# Mac fc:01:7c:9a:bb:d9 corresponde a la tarjeta interna
# Mac 50:3e:aa:2c:70:d0 corresponde a la tarjeta externa
# Dependencias: dzen2, wpa_supplicant
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org>

read -r STATE < /sys/class/net/wlan0/operstate

msg() {
	dzen2 -p 8 -e -fn 'JetBrains Mono:size=8:style=bold' -ta 5 \
		-w 260 -x 1100 -y 25
}

case "$1" in
	--toggle)
		if [ "$(pgrep wpa_supplicant)" ]; then
			doas pkill -f wpa_supplicant
			printf '%s\n' "Wifi desactivado" | msg &
		else
			if [ "$STATE" = "down" ]; then
				#doas wpa_supplicant -B -D wext -c /etc/wpa_supplicant/wpa_supplicant-wlan0.conf -i wlan1
				doas wpa_supplicant -B -D wext -c /etc/wpa_supplicant/wpa_supplicant-wlan0.conf -i wlan0
				sleep 2; printf '%s\n' "Activando wifi..." | msg &
			fi
		fi
		;;
	*)
		if [ "$(pgrep wpa_supplicant)" ]; then
			printf '%s\n' "Wifi activado" | msg &
		else
			printf '%s\n' "Wifi desactivado" | msg &
		fi
esac

