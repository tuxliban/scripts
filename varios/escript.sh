#!/bin/sh
#
# v1.0 - 19/09/2023
# Script para editar rápidamente otros script que se encuentren en el PATH de ejecutables o del PATH que se
# especifique manualmente
#
# Modo de uso:
#   escript filepath
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> - 2023

# Buscar el archivo en el PATH del sistema
file_path=$(command -v "$1" 2>/dev/null)

if [ -z "$file_path" ]; then
	# Si el archivo no existe en el PATH, mostrar un mensaje de error
	echo "Error: $1 no existe en el PATH de ejecutables"
	exit 2
else
	# Invocar $EDITOR si está definido, de lo contrario usar vi para editar el archivo si existe
	${EDITOR:-vi} "$file_path"
	exit 0
fi
