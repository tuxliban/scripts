#!/bin/sh
# Scrip que muestra la temperatura climática de la ciudad especificada
# Dependencias: curl
#
# Autor: O. Sánchez <o-sanchez@linuxmail.org>

CIUDAD=foo
CLIMA=$(curl -s wttr.in/$CIUDAD?format=1 | grep -o "[0-9].*")
echo " $CLIMA" > /tmp/weather
