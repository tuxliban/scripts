#!/bin/sh 
#
# v0.2 - 20/09/2023
# Menú con opciones en la terminal para poder apagar o reiniciar el sistema
#
# Dependencias: slock, xdotool
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2023

printf %s "Selecciona una opción: 
1. Apagar 
2. Reiniciar 
3. Bloquear 
4. Suspender 
5. Hibernar 
6. Logout 
7. Cancelar 

" 
read -r _option

case "$_option" in
	1|Apagar|apagar) 
		su - root -c 'shutdown -h now' 
		;; 
	2|Reiniciar|reiniciar) 
		su - root -c 'shutdown -r now' 
		;; 
	3|Bloquear|bloquear) 
		slock 
		;; 
	4|Suspender|suspender) 
		su - root -c 'zzz && slock' 
		;; 
	5|Hibernar|hibernar) 
		su - root -c 'ZZZ && slock' 
		;; 
	6|Logout|logout) 
		pkill startdwm && xdotool key "super+shift+q" 
		;; 
	7|Cancelar|*) 
	exit
esac
