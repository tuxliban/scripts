#!/bin/ksh
#
# v1.0 - 20/09/2023
# Versión Korn Shell de dirname
# O. Sánchez <o-sanchez@linuxmail.org> 2023
#

# Comprobar argumentos
if (($# ==0 || $# > 1)); then
	print "Uso: ${0##*/} string"
	exit 1
fi

# Obtener el nombre del directorio
print ${1%/*}
