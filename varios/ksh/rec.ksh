#!/bin/ksh
# v0.8 - 20/09/2023
# 
# Script para poder realizar screencast.
#
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2023
#
# Códigos de salida:
#	0  - Éxito 
#	10 - No se seleccionó ninguna opción
#	11 - Opción no válida
#

set -eu

# Obtener resolución de pantalla
read -r res < /sys/class/graphics/fb0/virtual_size

# Definir las funciones de grabación de audio y video
function video {
	 ffmpeg -y \
	-f x11grab \
	-framerate 60 \
	-s "${res/,/x}" \
	-i "$DISPLAY" \
	-c:v libx264 -profile:v baseline -level 3.0 -pix_fmt yuv420p \
	"$HOME/video-$(date '+%y%m%d-%H%M%S').mp4" &
	print $! >| /tmp/recordingpid_video
}

function audio {
	ffmpeg \
	-f alsa -i default \
	-c:a aac \
	"$HOME/audio-$(date '+%y%m%d-%H%M%S').aac" &
	print $! >| /tmp/recordingpid_audio
}

# Función para detener la grabación
function killrecording {
	audio_pid="$(cat /tmp/recordingpid_audio)"
	video_pid="$(cat /tmp/recordingpid_video)"

	[[ "$current_state" == "Grabando audio" ]] && kill -15 "$audio_pid"
	[[ "$current_state" == "Grabando video" ]] && kill -15 "$video_pid"
	[[ "$current_state" == "Grabando audio y video" ]] && kill -15 "$audio_pid" && kill -15 "$video_pid"
		rm -f /tmp/recordingpid_audio /tmp/recordingpid_video
}
# Ruta al archivo que guarda el estado actual
state_file="/tmp/recording_state.txt"

# Verificar si hay un estado previo almacenado
if [[ -f "$state_file" ]]; then
	current_state=$(cat "$state_file")

	# Mostrar opciones para finalizar la tarea actual o continuar
	choice=$(print "Continuar\nFinalizar" | dmenu -p "Tarea actual: $current_state")
  
	case "$choice" in
		Finalizar)
			# Detener la tarea actual
			killrecording
			rm "$state_file"
			exit 0
			;;
		Continuar)
			# Salir sin hacer nada más
			exit 10
			;;
	esac
fi

# Mostrar opciones iniciales para elegir qué grabar
action=$(print "Audio\nVideo\nAudio y video" | dmenu -p "Selecciona el modo de grabación")

case "$action" in
	Audio)
		# Ejecutar la función de grabación de audio y guardar el estado
		audio
		print "Grabando audio" > "$state_file"
		;;
	Video)
		# Ejecutar la función de grabación de video y guardar el estado
		video
		print "Grabando video" > "$state_file"
		;;
	"Audio y video")
		# Ejecutar ambas funciones de grabación y guardar el estado
		audio
		video
		print "Grabando audio y video" > "$state_file"
		;;
	*)
		print "Opción no válida"
		exit 11
		;;
esac
