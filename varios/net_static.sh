#!/bin/sh
#
# v0.2 - 19/09/2023
# Dependencias iproute, wpa_supplicant, dzen2, sudo, doas (opcional)
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2023

script="${0##*/}"

deps() {
if ! command -v wpa_supplicant; then
	printf '%b' "Dependencia no satisfecha:\n\twpa_supplicant\n"
	exit 1
elif ! command -v dzen2; then
	printf '%b' "Dependencia no satisfecha:\n\tdzen2\n"
	exit 1
fi
}


ayuda(){
printf %s "\
Script para establecer conexión a internet usando una dirección ip estática.

Uso: $script [interface]
 Ejemplo:
  $script eth0
  $script wlan0

"
}

admin() {
	if command -v sudo >/dev/null && sudo -l | grep -q -e ' ALL$' -e xbps-install; then
		echo sudo
	elif command -v doas >/dev/null && [ -f /etc/doas.conf ]; then
		echo doas
	elif [ "$(id -u)" != 0 ]; then
		echo su
	fi
}

msg() {
	dzen2 -p 5 -fn 'Inconsolata:size=10:style=bold' -ta 5 \
		-w 260 -x 1100 -y 25
}

_sudo=$(admin)

deps
case $1 in
	eth0)
		read -r state < /sys/class/net/"$1"/operstate
		if "$(pgrep wpa_supplicant)"; then
			"$_sudo" pkill -f wpa_supplicant
			if [ "$state" = "down" ]; then
				"$_sudo" ip link set "$1" up
				"$_sudo" ip a add 192.168.1.250/24 dev "$1"
				"$_sudo" ip route add default via 192.168.1.254 dev "$1"
				printf '%s\n' "Ethernet activado" | msg &
			fi
		elif [ "$state" = "down" ]; then
			"$_sudo" ip link set "$1" up
			"$_sudo" ip a add 192.168.1.250/24 dev "$1"
			"$_sudo" ip route add default via 192.168.1.254 dev "$1"
			printf '%s\n' "Ethernet activado" | msg &
		elif [  "$state" = "up" ]; then
			"$_sudo" ip link set eth0 down
			printf '%s\n' "Ethernet desactivado" | msg &
		fi
		;;
	wlan0)
		read -r state < /sys/class/net/"$1"/operstate
		read -r ethernet < /sys/class/net/eth0/operstate
		if [ "$ethernet" = "up" ]; then
			"$_sudo" ip link set eth0 down
			"$_sudo" ip a add 192.168.1.245/24 dev "$1"
			"$_sudo" ip route add default via 192.168.1.254 dev "$1"
			"$_sudo" wpa_supplicant -B -D nl80211,wext -c /etc/wpa_supplicant/wpa_supplicant-wlan0.conf -i "$1"
			printf '%s\n' "Wifi activado" | msg &
		elif [ "$state" = "down" ]; then
			"$_sudo" ip link set "$1" up
			"$_sudo" ip a add 192.168.1.245/24 dev "$1"
			"$_sudo" ip route add default via 192.168.1.254 dev "$1"
			"$_sudo" wpa_supplicant -B -D nl80211,wext -c /etc/wpa_supplicant/wpa_supplicant-wlan0.conf -i "$1"
			printf '%s\n' "Wifi activado" | msg &
		elif [ "$(pgrep wpa_supplicant)" ]; then
			"$_sudo" pkill -f wpa_supplicant
			"$_sudo" ip link set wlan0 down
			printf '%s\n' "Wifi desactivado" | msg &
		fi
		;;
	--help|-h|*)
		ayuda
esac
