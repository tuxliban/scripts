#!/bin/ksh
#
# v1.0 - 20/09/2023
# Version Korn Shell de cat
# O. Sánchez <o-sanchez@linuxmail.org> 2023
#

# Comprobar uso
if (($# < 1)); then
	print "Uso: ${0##*/} <archivo> ..."
	exit 1
fi

# Procesar cada fichero
while (($# > 0)); do
	# Asegurarse de que el archivo exista
	if [[ ! -f $1 ]]; then
		print "$1: Inexistente o no accesible"
	else
	
		# Abrir archivo de entrada
		exec 0<$1
		while read LINE; do
			# Visualización de salida
			print $LINE
		done
	fi

	# Obtener el argumento del siguiente archivo
	shift
done
