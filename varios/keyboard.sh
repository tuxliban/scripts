#!/bin/sh
#
# v1.0 - 19/09/2023
# Dependencias: xinput
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2023

script="${0##*/}"

ayuda(){
printf %s "\
Script para desactivar teclado interno en portátiles mientras se está en el servidor gráfico.

Uso: $script [arg]
 Ejemplo:
  $script on		Activa teclado interno
  $script off		Desactiva teclado interno
  $script --help, -h	Muestra este mensaje de ayuda

"
}

# Salir si existe un error
set -e

ID=$(xinput --list | awk '/Translated/ {print $7}' | cut -d '=' -f 2)

case "$1" in
	on)
		xinput set-int-prop "$ID" "Device Enabled" 8 1
		;;
	off)
		xinput set-int-prop "$ID" "Device Enabled" 8 0
		;;
	--help|-h|*)
		ayuda
esac
