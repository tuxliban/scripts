#!/bin/ksh
#
# v1.0 - 20/09/2023
# Versión Korn Shell de basename
# O. Sánchez <o-sanchez@linuxmail.org> 2023

# Comprobar argumentos
if (($# == 0 || $# > 2 )); then
	printf %s "\
Uso: ${0##*/} string [sufijo]
Ejemplos:
  ${0##*/} /path/foo.xyz
  foo.xyz\n

  ${0##*/} /path/foo .xyz
  foo

"
	exit 1
fi

# Obtener el nombre base
base=${1##*/}

# Ver si se ha dado el argumento sufijo
if (($# > 1 )); then
	# Mostrar nombre base sin sufijo
	print ${base%$2}
else
	# Mostrar nombre base
	print $base
fi
