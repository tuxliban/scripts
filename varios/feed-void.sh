#!/bin/sh
# Script que muestra notificaciones de paquetes nuevos en el repositorio remoto,
# actualizaciones remotas y actualizaciones disponibles para el sistema.
#
# Dependencias: rstail 
#
# Autor: O. Sánchez <o-sanchez@linuxmail.org>

if ! command -v rsstail >/dev/null; then
	printf "\033[31;5m[ERROR] Este  script requiere del paquete: 'rsstail'\033[0m\n"
	exit 1;
elif command -v rsstail >/dev/null; then
	rsstail -1 -u https://github.com/void-linux/void-packages/commits/master.atom > /tmp/feed
	updatedpkgs="$(grep -E "Update|update" /tmp/feed | wc -l)"
	newpkgs="$(grep -E "New|new" /tmp/feed | wc -l)"
	sysupdates="$(xbps-install -Mnu | wc -l)"

# Notificación emergente
  notify-send --urgency=critical """CAMBIOS EN EL REPOSITORIO:
  Paquetes actualizados: ${updatedpkgs}
  Nuevos paquetes: ${newpkgs}

ACTUALIZACIONES DE SISTEMA:
  Disponible actualmente: ${sysupdates}"""

# Notificación en consola
printf "\033[32;1mCAMBIOS EN EL REPOSITORIO\033[0m\n"
printf "\033[34;1m	Paquetes actualizados: \033[0m"${updatedpkgs}
printf "\n"
printf "\033[34;1m	Nuevos paquetes: \033[0m"${newpkgs}
printf "\n"
printf "\n"
printf "\033[32;1mACTUALIZACIONES DE SISTEMA:\033[0m\n"
printf "\033[33;5m	Disponible actualmente: \033[0m"${sysupdates}
printf "\n"
fi
