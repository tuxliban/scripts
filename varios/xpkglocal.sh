#!/bin/sh
# Script para consultar el sitio de origen del paquete (upstream) en busca de nuevas versiones.
# Se recomienda añadir este script en una tarea de crontab
# Dependencias: xbps-src, xtools, dzen2
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org>
# v1.0

set -u

TMP_DIR=$(mktemp -d /tmp/xpkglocal.XXXXXXXXXX)

which_sudo() {
	if command -v sudo >/dev/null && id | grep -q wheel; then
		echo sudo
	elif command -v doas >/dev/null && [ -f /etc/doas.conf ]; then
		echo doas
	elif [ "$(id -u)" != 0 ]; then
		echo su
	fi
}

admin=$(which_sudo)

do_install() {
	if [ "$admin" = su ]; then
		su root -c 'xbps-install "$@"' -- sh "$@"
	else
		$admin xbps-install "$@"
	fi
}

msg(){
	dzen2 -p -fn 'JetBrains Mono:size=8:style=bold' -ta 5 \
		-w 260 -x 1100 -y 25 -l 20
}

# Verificar que las dependencias estén instaladas
for cmd in xpkg dzen2; do
	if ! command -v $cmd >/dev/null 2>&1; then
		printf '%b' "\033[31;5m[ERROR] '$cmd' es requerido, instalando el paquete...\033[0m\n"
	fi
done

if [ ! -f "$HOME/void-packages/xbps-src" ]; then
	printf '%s\n' "No se encontró repositorio git de Void Linux. Clonadolo..."
	git -C "$HOME" clone --depth=1 https://github.com/void-linux/void-packages.git
	cd "$HOME"/void-packages || exit
	./xbps-src binary-bootstrap
	echo XBPS_ALLOW_RESTRICTED=yes >> etc/conf
fi

# Crear lista de actualizaciones disponibles
:> "${TMP_DIR}"/releases
cd "$HOME"/void-packages || exit

for package in $(xpkg -m); do
	./xbps-src update-check "$package" | tail -n 1 | tee -a "${TMP_DIR}"/releases > /dev/null
done

uniq "${TMP_DIR}"/releases > "${TMP_DIR}"/upstream_releases


UPDATES="$(wc -l < "${TMP_DIR}"/upstream_releases)"
RELEASES="$(awk '{print $1"\t"$3}' "${TMP_DIR}"/upstream_releases | column -t)"

if [ "$UPDATES" -gt 0 ]; then
	printf '%s\n' "$RELEASES"
	printf '%s\n' "ACTUALIZACIONES EN UPSTREAM: $UPDATES" "$RELEASES" | msg &
	rm -rf ${TMP_DIR}
	exit 0
else
	rm -rf ${TMP_DIR}
fi
