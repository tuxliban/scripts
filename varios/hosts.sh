#!/bin/sh
#
# V0.6 - 19/09/2023
# Script para descargar una lista personalizada con direcciones para bloquearlas 
# a través del fichero hosts. Para automatizar este proceso se recomienda crear 
# una tarea (crontab) y ajustarla a las necesidades del usuario (diario, semanal
# mensual, etc)
#
# Dependencias: dzen2, wget
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> - 2020 - 2023

msg() {
	dzen2 -p '10' -fn 'Inconsolata:size=10:style=bold' -ta '5' \
		 -w '260' -x '1100' -y '25'
}

which_sudo() {
	if [ "$(id -u)" -eq 0 ]; then
		return
	elif command -v sudo >/dev/null && id | grep -q wheel; then
		echo sudo
	elif command -v doas >/dev/null && [ -f /etc/doas.conf ]; then
		echo doas
	else
		echo su
	fi
}

admin=$(which_sudo)
                                                                                                        
# Realizar copia de seguridad del fichero hosts en caso de no existir
if [ ! -f /etc/hosts.bak ]; then
	printf '%b' "\033[32;1mCreando copia de seguridad del fichero hosts...\033[0m\n"
	$admin cp /etc/hosts /etc/hosts.bak
	printf "\033[33;1mCopia finalizada\033[0m\n"
else
	printf '%b' "\033[35;5mYa existe copia de seguridad del fichero hosts\033[0m\n"
fi

# Descargar actualización más reciente del fichero hosts
printf '%b' "\033[32;1mDescargando y copiando lista actualizada para fichero hosts...\033[0m\n"
wget -O /tmp/hosts http://sbc.io/hosts/alternates/fakenews-gambling-porn/hosts
"$HOME"/.local/bin/custom_sites
$admin mv /tmp/hosts /etc/hosts
"$HOME"/.local/bin/alert 2>/dev/null

# Notificacion de actualizacion del fichero
printf '%b' "\033[36;1mFichero hosts actualizado.\033[0m\n"
printf '%s\n' "Lista de fichero hosts actualizado" | msg &
