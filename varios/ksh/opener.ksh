#!/bin/ksh
#
# v0.5 - 4/11/2023
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2023
#
# Opener personalizado inspirado en el plugin 'nuke' de nnn.
#
# Dependencias:
#	pdf: zathura (GUI), pdftotext (CLI)
#	bsdtar: listar archivos comprimidos
#	audio: mocp (plugin nnn usando moc), mpv, exiftool (CLI)
#	avi|mkv|mp4: ffplay (GUI), exiftool (CLI)
#	log: $EDITOR (CLI)
#	csv|ods|xls|xlsx: sc-im (GUI), libreoffice (CLI)
#	odp|sxw|doc|ppt|pptx: libreoffice (CLI)
#	Markdown: glow (CLI)
#	htm|html|xhtml: w3m (CLI)
#	Imágenes: nsxiv (GUI), fbv (CLI)
#	text/troff: man (CLI)
#	text/* | */xml: $EDITOR (CLI)
#
# Códigos de salida
NO_ARGS=10
SUCCESS=15
FAILED_VIEWER=20
FAILED_EXTENSION=25

set -ef -o noclobber

if [[ ${#} == 0 ]]; then
	print "Uso: ${0##*/} <filepath>"
	exit $NO_ARGS
fi

IFS="$(printf '%b_' '\n')"; IFS="${IFS%_}" 
EDITOR="${VISUAL:-${EDITOR:-vi}}"
FILE_PATH="$1"
FILE_NAME="${1##*/}"
ext="${FILE_NAME##*.}"
if [[ -n $ext ]]; then
	typeset -l ext
	ext="$(print "$ext")"
fi

function check_tool {
	type "$1" >/dev/null 2>&1
}

# Función genérica para abrir archivos
function open_file {
	typeset viewer="$1"
	shift
	if type "$viewer" >/dev/null 2>&1; then
		"$viewer" "$@" "${FILE_PATH}" >/dev/null 2>&1 &
	fi
}

function open_with_converter {
	typeset converter="$1"
	shift
	if type "$converter" >/dev/null 2>&1; then
		"$converter" "$@" "${FILE_PATH}" | "$PAGER"
	fi
}

function open_with_editor {
	"$EDITOR" "${FILE_PATH}"
}


# Funciones para gestionar los archivos

function handle_pdf {
	check_tool zathura
	if [[ -n $DISPLAY ]]; then
		open_file zathura
	else
		pdftotext -l 10 -nopgbrk -q -- "${FILE_PATH}" - | "$PAGER"
	fi
}

function handle_audio {
	check_tool mocp
	check_tool mocq
	check_tool mpv
	check_tool exiftool
	if [[ -n $DISPLAY ]]; then
		mocq "${FILE_PATH}" opener || open_file mpv
	else
		print && open_with_converter exiftool
	fi
}

function handle_video {
	check_tool ffplay
	check_tool exiftool
	if [[ -n $DISPLAY ]]; then
		open_file ffplay -autoexit -fs
	else
		print && open_with_converter exiftool
	fi
}

function handle_image {
	check_tool nsxiv
	check_tool fbv
	if [[ -n $DISPLAY ]]; then
		open_file nsxiv -a
	else
		open_with_converter fbv -f
	fi
}

function handle_html {
	check_tool w3m
	if [[ -n $DISPLAY ]]; then
		open_with_converter w3m -dump
	else
		w3m -H -graph -no-cookie "${FILE_PATH}"
	fi
}

case "${ext}"  in
	## Archive
	a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|\
	rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zstd|zip)
		check_tool bsdtar
		open_with_converter bsdtar --list --file && exit $SUCCESS && exit $FAILED_VIEWER;;

	## PDF
	pdf)
		handle_pdf && exit $SUCCESS || exit $FAILED_VIEWER;;

	## Audio
	aac|flac|m4a|mid|midi|mpa|mp2|mp3|ogg|wav|webm|wma)
		handle_audio && exit $SUCCESS || exit $FAILED_VIEWER;;

	## Video
	avi|mkv|mp4|mov)
		handle_video && exit $SUCCESS || exit $FAILED_VIEWER;;

	## Image
	bmp|gif|jpeg|jpg|png|tiff|webp|heic|\
	postscript|jp2|jxl|avif|heif|eps)
		handle_image && exit $SUCCESS || exit $FAILED_VIEWER;;

	## Log files
	log)
		open_with_editor && exit $SUCCESS || exit $FAILED_VIEWER;;

	## Hojas de cálculo
	sc|csv|ods|xls|xlsx)
		check_tool sc-im
		if [[ -n $DISPLAY ]]; then
			open_file urxvtc -T "Terminal Sheets" -e sc-im && exit $SUCCESS || exit $FAILED_VIEWER
		else
			sc-im ${FILE_PATH} && exit $SUCCESS || exit $FAILED_VIEW
		fi;;

	## Documentos de texto
	odt|docx)
		check_tool pandoc
		open_with_converter pandoc -t plain && exit $SUCCESS || exit $FAILED_VIEWER;;

	## Winbugs documents y OpenDocument
	odp|sxw|doc|ppt|pptx)
		check_tool soffice
		soffice --nologo "${FILE_PATH}" >/dev/null 2>&1 && exit $SUCCESS || exit $FAILED_VIEWER;;

	## Markdown
	md)
		check_tool glow
		open_with_converter glow -sdark && exit $SUCCESS || exit $FAILED_VIEWER;;

	## HTML
	htm|html|xhtml)
		handle_html && exit $SUCCESS || exit $FAILED_VIEWER;;
esac

function handle_mime {
	typeset mimetype="${1}"
	case "${mimetype}" in
		## Manpages
		text/troff)
			man -a "${FILE_PATH}" && exit $SUCCESS || exit $FAILED_VIEWER;;

		## Text
		text/* | */xml | */javascript)
			open_with_editor && exit $SUCCESS || exit $FAILED_VIEWER;;

		*)
			print "Error: No se puede manejar el mimetype '${MIMETYPE}'"
			exit $FAILED_EXTENSION;;
	esac
}

MIMETYPE="$( file -bL --mime-type -- "${FILE_PATH}" )"
handle_mime "${MIMETYPE}"
