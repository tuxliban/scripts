#!/bin/sh
#
# v1.0 - 20/09/2023
# Script que revisa si existen actualizaciones disponibles para el sistema
# Dependencas: dzen2
#
# Lista de códigos de salida
#	0 - Operación exitosa
#	1 - Dependencia no cumplida
#	3 - Paquetes rotos
#	4 - Operación exitosa, pero no hubo cambios
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2020 - 2023

set -u

tmp_updates=$(mktemp -d /tmp/updates_void.XXXX)
xbps-install -nuM 1>"${tmp_updates}"/updates 2>"${tmp_updates}"/error

updates="$(awk 'END {print NR}' "${tmp_updates}"/updates)"
broken="$(grep -c broken "${tmp_updates}"/error)"
pkgs="$(awk '{printf "%-30s %s\n", $1, $2}' "${tmp_updates}"/updates)"

deps() {
  if ! command -v dzen2 >/dev/null; then
    printf '%b\n' "Dependencia no satisfecha:\n\tInstale dzen2\n"
    exit 1
  fi
}

msg() {
  dzen2 -p -fn 'Inconsolata:size=10:style=bold' -ta 5 \
    -w 260 -x 1100 -y 25 -l 10
}

if deps; then
  if [ "$broken" = 0 ] && [ "$updates" -ge 1 ]; then
    "$HOME"/.local/bin/alert
    printf '%s\n' "ACTUALIZACIONES DISPONIBLES: $updates" "$pkgs" | msg &
    rm -r "${tmp_updates}"
    exit 0
  elif [ "$broken" -ge 1 ]; then
    printf '%s\n' "HAY PAQUETES ROTOS" "$(awk '{printf "%-30s %s\n", $1, $5}' \
      "${tmp_updates}"/error)" | msg &
      rm -r "${tmp_updates}"
      exit 3
  else
    [ -z "$updates" ] || rm -r "${tmp_updates}"
    exit 4
  fi
fi
