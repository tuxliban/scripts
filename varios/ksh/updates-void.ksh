#!/bin/ksh
# v1.1 - 19/09/2023
#
# Script que revisa si existen actualizaciones disponibles para el sistema
# Dependencias: dzen2
#
# Lista de códigos de salida
#       0 - Operación exitosa
#       1 - Dependencia no cumplida
#       3 - Paquetes rotos
#       4 - Operación exitosa, pero no hubo cambios
#
# NOTA: 'countl' es una función personalizada de ksh
#
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2022 - 2023

tmp_updates=$(mktemp -d /tmp/updates_void.XXXX)
xbps-install -nuM 1>${tmp_updates}/updates 2>${tmp_updates}/error
updates="$(countl ${tmp_updates}/updates)"
broken="$(< ${tmp_updates}/error)"
pkgs="$(awk '{printf "%-25s %s\n", $1, $2}' ${tmp_updates}/updates)"


function deps {
	if ! command -v dzen2 >/dev/null; then
		printf '%b\n' "Dependencia no satisfecha:\n\tInstale dzen2\n"
		exit 1
	fi
}

function msg {
	dzen2 -p -fn 'Inconsolata:size=10:style=bold' -ta 5 \
		-w 260 -x 1100 -y 25 -l 10
}

if deps; then
	if [[ -z $broken && $updates > 0 ]]; then
		"$HOME"/.local/bin/alert 2>/dev/null
		printf '%s\n' "ACTUALIZACIONES DISPONIBLES: $updates" "$pkgs" | msg &
		rm -rf ${tmp_updates}
		exit 0
	elif [[ -n $broken ]]; then
		printf '%s\n' "HAY PAQUETES ROTOS" "$(awk '{printf "%-30s %s\n", $1, $5}' \
			${tmp_updates}/error)" | msg &
		rm -rf ${tmp_updates}
		exit 3
	else
		[[ $updates == 0 ]] && rm -rf ${tmp_updates}
		exit 4
	fi
fi
