#!/bin/sh
#
# v1.0 - 19/09/2023
# Script que muestra información sobre la canción en curso
#
# Dependencias: moc
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2020 - 2023

if [ "$(mocp -Q %state)" != "STOP" ];then
	SONG=$(mocp -Q %song)
	
	if [ -n "$SONG" ]; then
		echo " $SONG - $(mocp -Q %album)"
	else
#		basename " $(mocp -Q %file)"
		echo " $(mocp -Q %file)"
	fi
else
	echo ""
fi
