#!/bin/sh
#
# v0.1 - 15/09/2023
#
# Script que calcula la temperatura promedio del procesador leyendo la información
# disponible en el directorio /sys/class, esta información es proporcionada por 
# el kernel.
# El script realiza los cálculos apoyándose de herramientas del sistema y builtin
# del shell.
#
# Dependencias: cat bc 
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org>

# Directorio base donde se encuentran los archivos "temp"
base_dir="/sys/class/thermal"

# Inicializar variables para la suma y el contador
suma=0
contador=0

# Iterar sobre los directorios "thermal_zone*"
for dir in "$base_dir"/thermal_zone*; do
	if [ -d "$dir" ]; then
		# Verificar si el archivo "temp" existe en el directorio
		temp_file="$dir/temp"
		if [ -f "$temp_file" ]; then
			# Leer el contenido del archivo "temp" y agregarlo a la suma
			temp_value=$(cat "$temp_file")
			suma=$((suma + temp_value))
			contador=$((contador + 1))
		fi
	fi
done

# Calcular el promedio en enteros
if [ "$contador" -gt 0 ]; then
	promedio_enteros=$((suma / (contador * 1000)))

	# Calcular el promedio en decimal (con decimales)
	promedio_decimal=$(echo "scale=2; $suma / ($contador * 1000)" | bc)

	# Comprobar si el promedio tiene decimales y mostrarlo en consecuencia
	if [ "$promedio_enteros" -eq "$promedio_decimal" ]; then
		echo "${promedio_enteros##.*}"
		exit 0
	else
		echo "$promedio_decimal"
		exit 0
	fi
else
	echo "No se encontraron archivos 'temp' en los directorios 'thermal_zone*'."
	exit 10
fi
