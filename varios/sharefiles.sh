#!/bin/sh
#
# v0.4 - 15/09/2023
#
# Dependencias: nnn, preview-tabbed (plugin de nnn), xsel
#	- nnn (https://github.com/jarun/nnn): gestor de archivos de terminal
#	- preview-tabbed: previsualizador de archivos por pestañas/xembed
#	- xsel (https://github.com/kfish/xsel):  programa de línea de comandos para obtener y establecer 
#	el contenido de la selección X
#
# Cómo configurar el script:
#	- Asegurarse de tener el plugin preview-tabbed en el directorio $HOME/.config/nnn/plugins
#	(https://github.com/jarun/nnn/tree/master/plugins#list-of-plugins)
#	- Configure la variable de entorno de acuerdo a como se indica en la wiki de nnn 
#	(https://github.com/jarun/nnn/tree/master/plugins#configuration)
#	- Guarde este script en el path de ejecutables, por ejemplo en /usr/bin, /usr/local/bin,
#	o en $HOME/.local/bin (si no existe este directorio, hay que crearlo)
#	- Crear un atajo de teclado que para que ejecute este script
#
#
# Shell: POSIX compliant
#
# Autor: O. Sánchez <o-sanchez@linuxmail.org>
# Colaboración: Visone-Selektah <https://github.com/Visone-Selektah>

script="${0##*/}"

ayuda() {
printf '%s\n' "\

$script ayuda a explorar en busca de un archivo para compartir a través de
un link en los servidores de The Null Pointer o en transfer.sh 

Modo de uso:   
	$script <servidor>     
	
	--null        Carga y comparte archivo alojado en el servidor The Null Pointer
	--transfer    Carga y comparte archivo alojado en el servidor transfer.sh
	-h | --help   Muestra este mensaje de ayuda 

"
}

case $1 in
	
	# The Null Pointer Server
	--null)
		curl -F "file=@$(nnn -P p -dQ -p -)" -F "secret=" https://0x0.st | xsel -b
		;;
	
	# transfer.sh Server
	--transfer)
		files=$(nnn -P p -dQ -p -)
		curl --upload-file "$files" https://transfer.sh/"${files##/*}" | xsel -b
		;;
	-h|--help|*)
		ayuda
esac 
