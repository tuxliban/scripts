#!/bin/sh
#
# v1.5
# Script para sincronizar nube remota de dropbpox
# Dependencias: rclone, dzen2, rclonesync (opcional) 
#
# Lista de códigos de salida
#	0 - éxito
#	9 - operación exitosa, pero no se transfirieron archivos
#	10 - error temporal de sincronización
#	11 - error por falta de conexión
#	12 - error de dependencia
#	13 - error no categorizado de otra manera
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org>


set -u	# salir si una variable no ha sido declarada


########## Verificar que las dependencias estén instaladas ##########

deps() {
	for cmd in rclone dzen2; do
		if ! command -v $cmd >/dev/null 2>&1; then
			printf '%b' "\033[31;5m[ERROR] '$cmd' es requerido, instale el paquete\033[0m\n"
			return 12
		fi
	done

# El cliente rclone de forma nativa ya soporta le sincronización bidirecional, por esa razón
# las siguiente líneas de código han sido comentadas.

# if ! command -v rclonesync >/dev/null; then
	# echof '%b' "\033[31;5m[ERROR] No está disponible 'rclonesync' en el sistema. Clonado \
		# repositorio para instalar el ejecutable...\033[0m\n"
	# git -C /tmp clone https://github.com/cjnaz/rclonesync-V2.git --depth 1
	# [ -d "$HOME"/.local/bin ] || mkdir -p "$HOME/.local/bin"
	# cp /tmp/rclonesync-V2/rclonesync "$HOME"/.local/bin
	# export PATH="$HOME/.local/bin:$PATH"
	# return 12
# fi
}


########## Directorio para base de datos ##########

workdir="$HOME"/.config/rclone
tmpfile=$(mktemp /tmp/dropbox.XXXXXXXXXX)

if [ ! -f "$workdir"/dropbox.txt ]; then
	find "$HOME/Dropbox" -type f -exec md5sum {} \; > "$workdir"/dropbox.txt
fi


########## Eliminar algún archivo residual que pudiera haber quedado ##########

for _file in /tmp/dropbox*; do
	[ -f "$_file" ] || continue
	rm "$_file"
done


########## Sincronizar nube remota ##########

check_diff() {
	find "$HOME/Datos/Dropbox" -type f -exec md5sum {} \; > "$tmpfile"
	_diff=$(diff -q "$workdir/dropbox.txt" "$tmpfile")
	if [ -z "$_diff" ]; then
		return 9
	fi
}

# Rclone ya integra de forma nativa la sincronizacion bidireccional
# sync() {
# "$HOME"/.local/bin/rclonesync "$HOME"/Dropbox MiDropbox: \
	# --check-access \
	# --check-filename=RCLONE_TEST \
	# --filters-file "$HOME"/.config/rclone/Filtro.txt \
	# --max-deletes 75 \
	# --rclone-args \
	# --copy-links \
	# --multi-thread-streams=4 \
	# --transfers=8 >> "$HOME"/.config/rclone/rclonesync.log 2>&1
# }

# first_sync() {
# "$HOME"/.local/bin/rclonesync "$HOME"/Dropbox MiDropbox: \
	# --first-sync \
	# --check-access \
	# --check-filename=RCLONE_TEST \
	# --filters-file "$HOME"/.config/rclone/Filtro.txt \
	# --max-deletes 75 \
	# --rclone-args \
	# --copy-links \
	# --multi-thread-streams=4 \
	# --transfers=8 >> "$HOME"/.config/rclone/rclonesync.log 2>&1
# }

first_sync() {
	/usr/bin/rclone bisync "$HOME"/Dropbox MiDropbox: \
		--resync \
		--check-access \
		--check-filename=RCLONE_TEST \
		--filters-file "$HOME"/.config/rclone/Filtro.txt \
		--max-delete 75 \
		--remove-empty-dirs  \
		--color AUTO \
		--copy-links \
		--multi-thread-streams=4 \
		--transfers=8 \
		--log-file "$HOME"/.config/rclone/rclone.log 2>&1
}

sync() {
	/usr/bin/rclone bisync "$HOME"/Dropbox MiDropbox: \
		--check-access \
		--check-filename=RCLONE_TEST \
		--filters-file "$HOME"/.config/rclone/Filtro.txt \
		--max-delete 75 \
		--remove-empty-dirs \
		--color AUTO \
		--copy-links \
		--multi-thread-streams=4 \
		--transfers=8 \
		--log-file "$HOME"/.config/rclone/rclone.log 2>&1
}

msg() {
	dzen2 -p 8 -e 'onstart=uncollapse' -fn 'Inconsolata:size=10:style=bold' -ta c \
		-sa c -w 260 -x 1100 -y 25 -l 1
}


if deps; then
	if check_diff; then
		if ping -A -q -c 3 1.1.1.1 >/dev/null; then
			if sync; then
				echo "Dropbox sincronizado" | msg &
				echo "Dropbox sincronizado"
				mv "$tmpfile" "$workdir/dropbox.txt"
				exit 0
			else
				first_sync
				echo "Dropbox sincronizado" | msg &
				echo "Dropbox sincronizado"
				mv "$tmpfile" "$workdir/dropbox.txt" 
				exit 10
			fi
		elif [ "$(ping -A -c 3 1.1.1.1 >/dev/null)" = 1 ]; then
			printf '%s\n' "No se pudo sincronizar Dropbox" "Sin conexión a internet" | msg &
			printf '%s\n' "No se pudo sincronizar Dropbox. Sin conexión a internet"
			rm -f "$tmpfile"
			exit 11
		else
			rm -f "$tmpfile"
			printf '%s\n' "Ocurrieron errores" "Revisar el log" | msg &
			printf '%s\n' "Ocurrieron errores. Revisar el log"
			exit 13
		fi
	else
		rm -f "$tmpfile"
		printf '%s\n' "Operación exitosa, pero no se transfirieron archivos"
		exit 9
	fi
fi

printf %s "\
ERROR: Dependencias no satisfechas. Este script requiere tener instalado:
	rclone
	dzen2
"

exit 12
