#!/bin/sh
#
# v1.2 - 7/10/2023
# Script para convertir documentos de word a PDF o a texto plano usando pandoc
#
# NOTA: Este script hace uso de la sustitución de parámetros para obtener el nombre del archivo (basename)
# y de su respectivo path (dirname). Se aprovecha esta funcionalidad del shell para evitar hacer uso de
# comandos externos.
#
# Este script utiliza en su mayoría el parámetro $2 el cual puede ser el archivo que se desea convertir o
# también puede ser el directorio donde se encuentran los archivos que se convertirán a formato pdf.
#
# Equivalencias sustituyendo al parámetro $2:
#  1. extensión: ${2##*.} 
#  2. dirname: ${2%/*}
#  3. basename: ${2##*/}
#  4. pathfile sin extensión: ${2%.*} y ${_file%.*}
#
# Dependencias lualatex, pandoc
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> - 2023

set -e


# Nombre del script usando la sustitución de parámetros
script="${0##*/}"

# Revisar que las dependencias estén instaladas
deps() {
	if ! command -v lualatex >/dev/null 2>&1; then
		echo "Error: 'texlive-LuaTeX' es requerido, instale el paquete"
		return 1
	elif ! command -v pandoc >/dev/null 2>&1; then
		echo "Error: 'pandoc' es requerido, instale el paquete"
		return 1
	fi
}

ayuda() {
	printf "%s" "\
Nombre: $script
Propósito: Convertir documentos docx a formato PDF o a texto plano (txt)

Autor: O. Sánchez <o-sanchez@linuxmail.org> 2022 - 2023
Versión: 1.0 (25/05/2023)

  Uso: $script <arg> <directorio o archivo>
    --pdf | -p  	Convierte archivo(s) a formato PDF
    --txt | -t  	Convierte archivo(s) a texto plano
    --help | -h 	Mustra este mensaje de ayuda

EJEMPLOS:
  $script --pdf /path/foo.docx		Convierte archivo especificado
  $script -t /path/			Convierte archivos compatibles del directorio especificado a texto plano

"
}

if deps; then
	case $1 in
		--pdf|-p)
			# Si el segundo argumento existe y tiene terminación docx, realizar la conversión del documento
			# a archivo tipo PDF
			if [ -f "$2" ] && [ "${2##*.}" = "docx" ]; then
				printf "%s\n" "Convirtiendo ${2##*/} a formato PDF..."
				pandoc "$2" -V geometry:"top=2cm, bottom=1.5cm, left=2.4cm, right=2.4cm" \
					-V fontsize:"12pt" \
					-V papersize:"letter" \
					--pdf-engine=lualatex -o "${2%.*}.pdf"
				exit 0

			# Comprobar que el segundo argumento exista y que se trate de un directorio.
			elif [ -d "$2" ]; then
				# Si en el directorio hay archivos con extensión tipo .docx, convertirlos a archivo tipo PDF
				for _file in "$2"*.docx; do
					[ -f "$_file" ] || continue
					printf "%s\n" "Convirtiendo ${_file##*/} a formato PDF..."
					pandoc "$_file" -V geometry:"top=2cm, bottom=1.5cm, left=2.4cm, right=2.4cm" \
					-V fontsize:"12pt" \
					-V papersize:"letter" \
					 --pdf-engine=lualatex -o "${_file%.*}.pdf"
				done
				exit 0
			else
			  printf "%s" "\nERROR: el fichero o directorio no existe\n"
			  exit 1
			fi
			;;
		--txt|-t)
			if [ -f "$2" ] && [ "${2##*.}" = "docx" ]; then
				printf "%s\n" "Convirtiendo ${2##*/} a texto plano..." 
				pandoc -s "$2" -t plain -o "${2%.*}".txt --wrap=none
				exit 0
			elif [ -d "$2" ]; then
				for _file in "$2"*.docx; do
					[ -f "$_file" ] || continue
					printf "%s\n" "Convirtiendo ${_file##*/} a formato texto plano..."	
					pandoc "$_file" -t plain -o "${_file%.*}".txt --wrap=none
				done
				exit 0
			else
				printf "%s" "\nERROR: el fichero o directorio no existe\n"
				exit 1
			fi
			;;
		--help|-h|*)
			ayuda
	esac
fi
