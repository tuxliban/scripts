#!/bin/ksh
#
# v0.6 - 20/09/2023
# Dependencias:ImageMagick, xsel, dzen2, xdotool
#
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2023

script="${0##*/}"

_dir="$HOME"/Datos/Capturas
_date="$(date +%Y%m%d-%H%M%S)"
_admin=$(which_sudo)

for package in ImageMagick xclip dzen2 xdotool; do
	if ! command -v $package > /dev/null; then
	printf '%b' "\033[31;5m[ERROR] Dependencias no satisfecha. Instalando $package...\033[0m\n"
	$_admin xbps-install -Sy $package
	fi
done

function ayuda {
printf %s "\
Script para realizar capturas de pantalla utilizando ImageMagick.

Modo de uso:
 $script [-PsfgSFh]
   -P		Guardar captura de pantalla completa en el portapapeles
   -s		Guardar área seleccionada en el portapapeles
   -f		Guardar captura de pantalla de ventana activa en el portapapeles
   -g		Guardar captura de pantalla completa en el disco duro
   -S		Guardar área seleccionada en el disco duro
   -F 		Guardar captura de pantalla de ventana activa en el disco duro
   --help | -h	Mostrar este mensaje de ayuda

"
}

function msg {
 dzen2 -p 8 -e 'onstart=uncollapse' -fn 'Inconsolata:size=10:style=bold' -ta 5 \
   -sa c -w 260 -x 1100 -y 25 -l 1
}

case "$1" in
	-p)
		import -window root png:- | xclip -t 'image/png' -selection 'clipboard' -i
		;;
	-s)
		sleep 1 && import png:- | xclip -t 'image/png' -selection 'clipboard' -i
		;;
	-f)
		import -window "$(xdotool getwindowfocus)" png:- | xclip -t 'image/png' -selection 'clipboard' -i
		;;
	-g)
		[[ ! -d $_dir ]] || mkdir "$_dir"
		import -format png -window root "$_dir/$_date.png"
		"$HOME"/.local/bin/alert
		printf '%s\n' "CAPTURA DE PANTALLA" "Guardando en: ~/Datos/Capturas" | msg &
		;;
	-S)
		[[ ! -d $_dir/Select ]] || mkdir -p "$_dir"/Select
		sleep 1 && import -format png "$_dir/Select/select-$_date.png"
		"$HOME"/.local/bin/alert
		printf '%s\n' "ÁREA SELECCIONADA" "Guardando en: ~/Datos/Capturas/select" | msg &
		;;
	-F)
		[[ ! -d $_dir/Select ]] || mkdir -p "$_dir"/Select
		"$HOME"/.local/bin/alert
		import -window "$(xdotool getwindowfocus)" -format png "$_dir/Select/window-$_date.png"
		;;
	--help|-h|*)
		ayuda
esac
