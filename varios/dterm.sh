#!/bin/sh

# Iniciar urxvtd automáticamente cada vez que se ejecute urxvtc
# y el demonio aún no se esté ejecutando.
# Este script intentará crear una nueva terminal, y si falla con el estado de salida 2,
# lo que significa que no se pudo conectar al demonio, lo iniciará y volverá a ejecutar el comando.
# Las invocaciones posteriores del script reutilizarán el demonio existente

urxvtc -title "Enter The Void" "$@"
if [ $? -eq 2 ]; then
	urxvtd -q -o -f
	urxvtc -title "Enter The Void" "$@"
fi
