#!/bin/sh
#
# v0.2 - 19/09/2023
# Dependencias sdhcp, wpa_supplicant, dzen2, sudo, doas (opcional)
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2023

script="${0##*/}"

deps() {
	if ! command -v sdhcp; then
		printf '%b\n' "Dependencias no satisfecha:\n\tsdhcp\n"
		exit 1
	elif ! command wpa_supplicant; then
		printf '%b' "Dependencia no satisfecha:\n\twpa_supplicant\n"
		exit 1
	elif ! command -v dzen2; then
		printf '%b' "Dependencia no satisfecha:\n\tdzen2\n"
		exit 1
	fi
}

ayuda(){
printf %s "\
Script para asignar ip dinámicamente usando sdhcp para establecer conexión a internet.

Uso: $script [interface]
 Ejemplo:
  $script eth0
  $script wlan0

"
}

admin() {
	if command -v sudo >/dev/null && sudo -l | grep -q -e ' ALL$' -e xbps-install; then
		echo sudo
	elif command -v doas >/dev/null && [ -f /etc/doas.conf ]; then
		echo doas
	elif [ "$(id -u)" != 0 ]; then
		echo su
	fi
}

msg() {
	dzen2 -p 5 -fn 'Inconsolata:size=10:style=bold' -ta 5 \
		-w 260 -x 1100 -y 25
}

_sudo=$(admin)

deps
case $1 in
	eth0)
		read -r state < /sys/class/net/"$1"/operstate
		if [ "$state" = "down" ]; then
			"$_sudo" ip link set "$1" up
			"$_sudo" sdhcp "$1"
			printf '%s\n' "Ethernet activado" | msg &
		elif [  "$state" = "up" ]; then
			"$_sudo" kill "$(pgrep --list-full sdhcp | awk '/eth0/ {print $1}')"
			"$_sudo" ip link set eth0 down
			printf '%s\n' "Ethernet desactivado" | msg &
		fi
		;;
	wlan0|wlan1)
		read -r state < /sys/class/net/"$1"/operstate
		if [ "$(pgrep wpa_supplicant)" ]; then
			"$_sudo" pkill -f wpa_supplicant
			"$_sudo" ip link set "$1" down
			"$_sudo" kill "$(pgrep --list-full sdhcp | awk '/wlan/ {print $1}')"
			printf '%s\n' "Wifi desactivado" | msg &
		elif [ "$state" = "down" ]; then
			echo Activando interfaz
			"$_sudo" ip link set "$1" up
			echo Iniciando wpa_supplicant...
			"$_sudo" wpa_supplicant -B -D nl80211,wext -c /etc/wpa_supplicant/wpa_supplicant-wlan0.conf -i "$1"
			echo Asignando ip dinámica...
			printf '%s\n' "Activando wifi..." | msg &
			"$_sudo" sdhcp "$1"
		fi
		;;
	--help|-h|*)
		ayuda
esac
