#!/bin/sh
# Script que busca formatos compatibles *.gif *.jpeg *.jpg *.png y los muestra en la pantalla
# Dependencias: sxiv o nsxiv

if command -v sxiv; then
	sxiv -pa "$@" 2>/dev/null &
elif command -v nsxiv; then
	nsxiv -pa "$@" 2>/dev/null &
fi
