#!/bin/sh
#
# v1.0 - 19/09/2023
# Códigos de salida:
#	0 - Operación exitosa
#	3 - Operación exitosa, pero no se seleccionó un archivo
#	
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2023

#_path="$HOME"
_path="$PWD"
book=$(find "$_path" -name '*.pdf' -printf '%f\n' | dmenu -l 10 -p "Library")
_book=$(find "$_path" -name "$book")

if [ -z "$book" ]; then
	exit 3
else
	zathura "$_book" &
	exit 0
fi
