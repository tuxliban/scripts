#!/bin/sh
# Script que muestra el porcentaje activo del volumen del sistema
#
# Dependencias: alsa-utils
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org>

STATUS=$(amixer get Master | awk -F'[][]' 'END{ print $6 }')
LEVEL=$(amixer get Master | awk -F'[][]' 'END{ print $2 }')

if [ "$STATUS" = "on" ]; then
	echo "VOL $LEVEL"
else 
	echo "VOL mute";
fi
