#!/bin/ksh
#
# v1.1 - 25/10/2023
# Script para consultar el sitio de origen del paquete (upstream) en busca de nuevas versiones.
# Se recomienda añadir este script en una tarea de crontab
# Dependencias: dzen2, curl, xtools
#
# Autor: O.Sánchez <o-sanchez@linuxmail.org> 2023

set -u

if [[ ${#} == 0 ]]; then
	printf '%s\n' "Uso: ${0##*/} <opción>
    -n    Muestra actualizaciones a través de una notificación
    -t    Muestra las actualizaciones en la terminal"
	exit 1
fi

upstream=$(mktemp "$HOME"/.cache/up.XXXX)
list=https://repo-default.voidlinux.org/void-updates/void-updates.txt

function msg {
	dzen2 -p -fn 'Inconsolata:size=10:style=bold' -ta 5 \
		-w 260 -x 1100 -y 25 -l 20
}

# Muestra la lista de actualizaciones en un notificación
function notification {
	# Verificar que las dependencias estén instaladas
	if ! command -v dzen2 >/dev/null 2>&1; then
		printf '%b' "\033[31;5m[ERROR] 'dzen2' es requerido. Instale el paquete...\033[0m\n"
		exit 1
	fi

	# Descargar lista de actualizaciones disponibles
	curl -s ${list} > ${upstream}

	# Filtrar actualizaciones en upstream disponibles
	xpkg -m | sed 's/^/^/; s/$/ /' | grep -w -f - ${upstream} | awk '{print $1" "$2" --> "$4}' \
		| sort -k4rn | sort -uk1,1 > "$HOME"/.cache/updates.txt

	updates="$(countl $HOME/.cache/updates.txt)"

	if [[ -n $updates ]]; then
		$HOME/.local/bin/alert 2>/dev/null
		printf '%s\n' "ACTUALIZACIONES EN UPSTREAM: $updates" "$(< "$HOME"/.cache/updates.txt)" | msg &
		rm ${upstream}
		rm "$HOME"/.cache/updates.txt
		exit 0
	else
		rm ${upstream}
	fi
}

# Muestra la lista de actualizaciones en la terminal
function terminal {
	curl -s ${list} > ${upstream}
	xpkg | sed 's/^/^/; s/$/ /' | grep -w -f - ${upstream} | awk '{print $1"\t"$2" --> "$4}' \
		| sort -k4rn | sort -uk1,1
	rm ${upstream}
}

case "$1" in
	-n)
		notification ;;
	-t)
		terminal
esac
