#!/bin/sh
#
# v1.0 - 19/09/2023
# Script para hacer scripts de Shell más rápido.
# Al guardar los cambios del script y cerrarlo, se asignan los permisos de ejecución e inmediatamente se 
# ejecuta el script para verificar que funcione. Si fuera necesario continuar editando basta con presionar la
# tecla ENTER para regresar al editor; si por el contrario ya no se continuará editando el script, basta con
# presionar la combinación de teclas de Ctrl-C para finalizar el proceso.
#
# Modo de empleo:
#   do_script filepath
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2023

_usage() {
	echo "Usage: ${0##*/} new_file"
	exit 1
}

if [ ${#} -eq 0 ]; then
	_usage
fi

# Validar que el archivo proporcionado como argumento existe y es válido
if [ ! -f "$1" ]; then
	echo "#!/bin/sh" > "$1"
fi

while true; do
	${EDITOR:-vi} "$1"
	if [ ! -x "$1" ]; then
	  chmod u+x "$1"
	  ./"$1"
	else
	  ./"$1"
	fi
	
	# Presionar Enter para continuar o Ctrl-C para salir
	read -r dummy
done
