#!/bin/ksh
#
# Códigos de salida
#	0	Éxito (batería baja)
#	3	Suficiente bateria
#
# Dependencias: dzen2
#
# Autor: Tuxliban Torvalds <o-sanchez@linuxmail.org>

read -r capacity < /sys/class/power_supply/BAT0/capacity
read -r status < /sys/class/power_supply/BAT0/status

function dep {
	if ! command -v dzen2 >/dev/null; then
		printf '%b\n' "Dependencia no satisfecha:\n\tInstale dzen2\n"
		exit 1
	fi
}

function msg {
	dzen2 -p -fn 'JetBrains Mono:size=8:style=bold' -ta 5 \
		-w 260 -x 1100 -y 25
}

if [[ "$status" == "Discharging" ]]; then
	if (( capacity <= 25 )); then
		"$HOME"/Dropbox/Gitea/scripts/varios/dunst_sound 2> /dev/null
		printf '%s\n' "Batería baja: $capacity%, conectar el cargador" | msg &
		exit 0
	fi
fi
exit 3
