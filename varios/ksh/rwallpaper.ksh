#!/bin/ksh
# v1.1 - 20/09/2023
#
# Script que aleatoriza wallpaper
# Dependencias: ImageMagick
#
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2023
#
# Lista de códigos de salida
#	0 - éxito
#	10 - Dependencia no satisfecha
#	11 - Wallpaper no se modificó

function deps {
	# Si no se encuentra el comando "magick" (que es parte de ImageMagick) en el sistema, 
	# mostrar un mensaje de error
	if ! command -v magick >/dev/null; then
		printf '%s\n' "ImageMagick no disponible, instálelo"
		return 10
	fi
}

# Obtener la resolución de la pantalla
read -r resolution < /sys/class/graphics/fb0/virtual_size

dir_wall="$HOME/Datos/Imágenes/Wallpapers"

# Arreglo que contiene los wallpapers en el directorio dir_wall con extensiones .jpg, .jpeg, .png y webp
#wall=("$dir_wall"/*.{jpg,jpeg,png,webp})
wall=("$dir_wall"/*.webp)

# Obtener un número aleatorio entre 0 y la cantidad de archivos de imagen en la lista
((rand_index=RANDOM % ${#wall[@]}))

# Obtener la ruta completa del wallpaper seleccionado aleatoriamente
rand_img="${wall[$rand_index]}"

# Verificar si ImageMagick está instalado
if deps; then
	# Comprobar que el archivo seleccionado exista
	if [[ -f $rand_img ]]; then
	
		# Si el archivo existe, configurarlo como wallpaper
		display -resize "!${resolution/,/x}" -window root "$rand_img"
		exit 0
	else
		# Si el archivo no existe, mostrar un mensaje de error
		print "El archivo $rand_img no existe."

		# Obtener un índice aleatorio nuevamente
		if [[ ! -f $rand_img ]]; then
			((rand_index=RANDOM % ${#wall[@]}))
			rand_img="${wall[$rand_index]}"
			display -resize "!${resolution/,/x}" -window root "$rand_img"
		fi

		# Devolver un código de salida 11 (Wallpaper no se modificó)
		exit 11
	fi
else

	# Si ImageMagick no está instalado, devolver un código de salida 10 (Dependencia no satisfecha)
	exit 10
fi
