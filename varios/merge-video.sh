#!/bin/sh
# 
# NOTA: este script aún está en fase de pruebas
#	Convertir audio ogg a aac
#		ffmpeg -i input.ogg -strict experimental -acodec aac output.aac
#	
#	Añadir audio a video
#		ffmpeg -i video.mp4 -i audio.mp3 -c:v copy -c:a copy videoconaudio.mp4
#	
#	Cortar video
#		Con -ss estoy indicando que tome el video a partir del minuto 00:05:10 y luego 
#		con -to 00:15:30 indico que relice un corte de video con duración de 10 minutos
#		y 20 segundos
#		ffmpeg -i input.mp4 -ss 00:05:10 -to 00:15:30 -c:v copy -c:a copy output2.mp4
#	
#	Para quitar audio a un video hacerlo del siguiente modo:
#		ffmpeg -i $input_file -c copy -an $output_file
#
#	Para unir videos primero crear un archivo de texto con el nombre de los videos a unir:
#		vim lista.txt
#		file 1.mp4
#		file 2.mp4
#	
#	Unir los videos:
#		ffmpeg -f concat -i lista.txt -c copy output.mp4
#
# Dependencias: ffmpeg, find
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org>

script="${0##*/}"

ayuda() {
printf %s "\
Modo de uso: $script [OPCIÓN] [FICHERO, /path]

Ejemplos:
	$script -c /path/file START END
	$script --merge /path

Selección e interpretación de opciones:
  -c, --cut		Crea un clip cuya duaración estará basada en el punto de inicio y el punto final que
  			se indique del video de origen.
  -m, --merge		Fusiona audio y video 
  -h, --help		Muestra esta ayuda y sale.

NOTA: Los argumentos START y END espeficarlos en formato de tiempo, ejemplo: 00:10:15 (horas, minutos y segundos)

"
}

case "$1" in
	--cut|-c)
		ffmpeg -i "$2" -ss "$3" -to "$4" -c:v copy -c:a copy "$(date '+%y%m%d_%H%M%S').mp4"
		;;
	--merge|-m)
		if [ -n "$(count $2/*.oga)" ] && [ -n "$(count $2/*.mp4)" ]; then
			cd $2 || exit
			mv ./*.oga input.oga && mv video*.mp4 input.mp4

			ffmpeg -i input.oga -strict experimental -acodec aac audio.aac
			ffmpeg -i input.mp4 -i audio.aac -c:v copy -c:a copy "$(date '+%y%m%d_%H%M%S').mp4"

			mv input.* audio.aac "$HOME"/.local/share/Trash/files
		fi
		;;
	--help|-h|*)
		ayuda
esac
