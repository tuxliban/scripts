#!/bin/sh
# v2.2 - 20/09/2023
# Dependencias: udevil, dzen2
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2021 - 2023

script="${0##*/}"

ayuda() {
printf %s "\
Script para montar y desmontar dispositivos a través de devmon

Modo de uso:

 $script [-muUh]

Opciones:
 -m	Montar dispositivo extraible
 -u	Desmontar dispositivo extraible
 -U	Desmontar último pendrive insertado
 -h	Mostrar ayuda

"
}

admin(){
	if [ "$(id -u)" = "0" ]; then
		return
	elif command -v sudo >/dev/null && sudo -l | grep -q -e ' ALL$' -e xbps-install; then
		echo sudo
	elif command -v doas >/dev/null && [ -f /etc/doas.conf ]; then
		echo doas
	else
		echo su
	fi
}

do_install() {
	if [ "$_sudo" = su ]; then
		su root -c 'xbps-install "$@"' -- sh "$@"
	else
		$_sudo xbps-install "$@"
	fi
}

_sudo=$(admin)

if ! command -v udevil >/dev/null; then
	printf '%b' "\033[31;5m[ERROR] No se encontró instalado el paquete 'udevil'\033[0m\n"
	printf '%b' "\033[31;1mPreparándose para instalar el paquete 'udevil'...\033[0m\n"
	do_install -Sy udevil
elif ! command -v dzen2 >/dev/null; then
	printf '%b' "\033[31;5m[ERROR] No se encontró instalado el paquete 'udevil'\033[0m\n"
	printf '%b' "\033[31;1mPreparándose para instalar el paquete 'udevil'...\033[0m\n"
	do_install -Sy dzen2
fi

msg() {
	dzen2 -p 8 -e 'onstart=uncollapse' -fn 'Inconsolata:size=10:style=bold' -ta c \
		-sa c -w 260 -x 1100 -y 25 -l 1
}

case $1 in
	-m)
		# Montar dispositivo extraible
		devmon --sync --exec-on-device %f "printf '%s\n' 'Dispositivo USB' 'Listo para usarse'" | msg &
#		if [ -d /media/"$USER"/* ]; then
#			devmon --sync --exec-on-drive "printf '%s\n' 'DISPOSITIVO USB' 'Listo para utilizarse'" | msg &
#		fi
		break
		;;
	-u)
		# Desmontar dispositivo
		devmon --unmount /media/"$USER"/* && pkill -9 udevil
		sleep 2; printf '%s\n' "DISPOSITIVO USB" "Puede retirarlo con seguridad" | msg &
		break
		;;
	-U)
		# Desmontar último pendrive insertado
		devmon --unmount-recent && sleep 2; printf '%s\n' "DISPOSITIVO USB" "Puede retirarlo con seguridad" | msg &
		break
		;;
	-h|--help|*)
		ayuda
esac
exit 0
