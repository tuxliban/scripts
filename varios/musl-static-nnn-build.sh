#!/bin/sh
BIN=nnn.static-musl

# Instalar paquetes básicos de desarrollo
doas xbps-install -Sy pkgconf gcc libtool automake

# Crear directorio donde se gurdarán las bibliotecas estáticas para compilar nnn
if [ ! -d "./nnn-libs" ]; then
	mkdir nnn-libs
else
	rm -vf nnn-libs/*
fi

# Obtener netbsd-curses y compilar bibliotecas estáticas
[ ! -d "./netbsd-curses" ] && git clone https://github.com/sabotage-linux/netbsd-curses
cd netbsd-curses || exit
git checkout v0.3.2
make CC=gcc CFLAGS="-O3 -fPIC" LDFLAGS=-static all-static -j$(($(nproc)+1))
cp libcurses/libcurses.a libterminfo/libterminfo.a ../nnn-libs/
make clean
cd ..

# Compilar la biblioteca estática de musl-fts
[ ! -d "./musl-fts" ] && git clone https://github.com/void-linux/musl-fts --depth=1
cd musl-fts || exit
./bootstrap.sh
./configure
make CC=gcc CFLAGS=-O3 LDFLAGS=-static -j$(($(nproc)+1))
cp .libs/libfts.a ../nnn-libs/
cd ..

# Compilar nnn estáticamente
[ -e "./netbsd-curses" ] || rm "$BIN"
gcc -O3 -DNORL -DNOMOUSE -std=c11 -Wall -Wextra -Wshadow -I./netbsd-curses/libcurses -I./musl-fts -o "$BIN" \
	src/nnn.c -Wl,-Bsymbolic-functions -lpthread -L./nnn-libs -lcurses -lterminfo -lfts -static

# Despojar binario de símbolos de depuración
strip "$BIN"

doas xbps-remove -Ry pkgconf gcc libtool automake
doas rm -r netbsd-curses musl-fts
