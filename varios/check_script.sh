#!/bin/sh
# Script que revisa la sintaxis en busca de bashismos y que sea compatible con el estándar POSIX
#
# Dependencias: shellcheck, checkbashisms
#
# Uso:
# check_script filepath
#
# Shell: POSIX compliant
# Autor: O. Sánchez <o-sanchez@linuxmail.org>

if ! command -v shellcheck >/dev/null; then
	printf '%b' "\033[31;5m[ERROR] Instale el paquete 'shellcheck'\033[0m\n"
elif ! command -v checkbashisms >/dev/null; then
	printf '%b' "\033[31;5m[ERROR] Instale el paquete 'checkbashisms'\033[0m\n"
fi

glibc shellcheck "$1" 
checkbashisms "$1"
