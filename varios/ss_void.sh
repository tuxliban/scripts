#!/bin/sh
#
# v1.4 - 20/09/2023
# Script para realizar capturas de pantalla
# Dependencias: scrot, xclip, dzen2
#
# Autor: O. Sánchez <o-sanchez@linuxmail.org> 2020 - 2023

script="${0##*/}"

ayuda() {
printf %s "\
Script para realizar capturas de pantalla.
Modo de uso:
 $script [-PSgsh]

Opciones:
 -P:	Guardar captura de pantalla en portapapeles
 -S:	Guardar captura de pantalla del área seleccionada en el portapapeles
 -g:	Guardar captura de pantalla en disco duro
 -s:	Guardar captura de pantalla de área seleccionada en disco duro
 -h:	Mostrar ayuda

"
}

msg() {
		  dzen2 -p 8 -e 'onstart=uncollapse' -fn 'Inconsolata:size=10:style=bold' -ta 5 \
					 -sa c -w 260 -x 1100 -y 25 -l 1
}

case $1 in
	-P)
		scrot /tmp/'%F_%T.png' -e 'xclip -selection c -t image/png < $f'
		;;
	-S)
		sleep 1
		scrot --line style=dash,width=1,color="red" -s /tmp/'%F_%T.png' -e 'xclip -selection c -t image/png < $f'
		;;
	-g)
		scrot -q 100 '%F_%H%M%S_$wx$h.png' -e 'mv $f /home/skynet/Datos/Capturas/'
		sleep 1
	
		# Notificación
		"$HOME"/.local/bin/alert
		printf '%s\n' "CAPTURA DE PANTALLA" "Guardando en: ~/Datos/Capturas" | msg
		;;
	-s)
		# Guardar captura de pantalla de área seleccionada
		sleep 1
		scrot --line style=dash,width=1,color="red" -s -q 50 'Select_%F_%H%M%S_$wx$h.png' -e 'mv $f /home/skynet/Datos/Capturas/select'

		# Notificación
		"$HOME"/.local/bin/alert
		printf '%s\n' "CAPTURA DE PANTALLA" "Guardando en: ~/Datos/Capturas/select" | msg
		;;
	-h|--help)
		ayuda
		;;
	*)
		printf "\033[31;5mOpción inválida\033[0m\n\n"
		printf "\033[37;2m   Opciones disponibles:\033[0m\n"
		printf "\033[32;1m	-P:   \033[36;2mGuardar captura de pantalla en portapapeles\033[0m\\033[0m\n"
		printf "\033[32;1m	-S:   \033[36;2mGuardar captura de pantalla del área seleccionada en el portapapeles\033[0m\\033[0m\n"
		printf "\033[32;1m	-g:   \033[36;2mGuardar captura de pantalla en disco duro\033[0m\\033[0m\n"
		printf "\033[32;1m	-s:   \033[36;2mGuardar captura de pantalla de área seleccionada en disco duro\033[0m\\033[0m\n\n"
		return
esac

# Si no existe el binario scrot
if ! command -v scrot; then
	printf "\033[31;5m[ERROR] No se encontró instalado el paquete 'scrot'\033[0m\n"

# Si no existe el binario xclip
elif ! command -v xclip; then
	printf "\033[31;5m[ERROR] No se encontró instalado el paquete 'xclip'\033[0m\n"

# Si no existe el binario dzen2
elif ! command -v dzen2; then
	printf "\033[31;5m[ERROR] No se encontró instalado el paquete 'dzen2'\033[0m\n"
fi
