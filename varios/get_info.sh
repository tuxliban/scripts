#!/bin/sh
## Scrip para obtener información de las ventanas activas: class, instance
## Dependencias xprop

xprop | awk '
	/^WM_CLASS/{sub(/.* =/, "instance:"); sub(/,/,"\nclass:"); print}
	/^WN_NAME/{sub(/.* =/, "title:"); print}'
